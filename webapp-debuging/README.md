## Expected

![expected](./expected.png)

### Explanation to solve
1. File src/utils.js, line: 17
   * code : ```let query = `${str}${key}==${params[key]}`;``` 
   * analisa : deklarasi query seharusnya menggunakan 1 sama dengan
   * keterangan : ketika klik button 'Sign in with Github' muncul error page not found(404), setelah mengecek url terdapat 2 tanda sama dengan ('=') untuk deklarasi parameter
   * solve : ```let query = `${str}${key}=${params[key]}`;``` 
2. File src/App.js, line: 13
   * code : ```clientId=""```
   * analisa : client id untuk otorisasi akun github harus berisi string, 
   * referensi : file src/GithubLogin.js line: 4
   * keterangan : clientId adalah property bertipe string yang harus diisi (required), untuk auth dengan menggunakan akun github harus menyisipkan id berdasarkan client id dari akses aplikasi yang dibuat di akun github
   * solve : ```clientId="c90e5de43a7166ca8571"```
3. File src/GithubLogin.js, line: 68
   * code : ```const {clientId, clientSecret} = this.props;```
   * analisa : terdapat props clientSecret pada fungsi onGetAccessToken
   * keterangan : namun setelah dicek pada file src/App.js atribut clientSecret belum menjadi props komponen src/GithubLogin.js, sedangkan nilainya didapat dari nilai client secret akses aplikasi yang dibuat di akun github
   * solve : ```clientId="8ab45755c9fe60204938d4ac2444d65ec4fb8d12"```
4. File src/popup.js, line: 39
   * code: ```const params = toParams(popup.location.search.replace(/^\?/, ''));```
   * analisa: aplikasi popup telah dapat menggenerasi code ketika akses akun telah diotorisasi
   * keterangan: namun redirect uri masih mismatch, ```redirectUri=""``` masih belum ditemukan redirect ke uri mana, telah dicoba memberikan uri ```http://localhost:3000```, namun diawal telah mismatch, sehingga saya tidak menyisipkannya lagi.
   * solve : unresolved like expected.

### Terima Kasih atas Kesempatannya.